package taller.mundo;

import taller.estructuras.Heap;


public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 * 
	 */
	private Heap<Pedido> recibidos; 
	/**
	 * Getter de pedidos recibidos
	 */
	// TODO 
 	/** 
	 * Heap de elementos por despachar
	 */
	private Heap<Pedido> desp;
	/**
	 * Getter de elementos por despachar
	 */
	// TODO 
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		recibidos= new  Heap<Pedido>(10, true);
		desp= new Heap<Pedido>(10, false);
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		recibidos.add(new Pedido(precio, nombreAutor, cercania));
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO 
		Pedido p= recibidos.poll();
		if(p!=null){
		p.atender();
		desp.add(p);}
		return p;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		
	    return desp.poll();
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido[] pedidosRecibidosList()
     {
        //System.out.println(((Pedido)(recibidos.darCosas()[1])).getAutorPedido());
        Object[] o=recibidos.darCosas();
    	 Pedido[] r= new Pedido[o.length];
    	 for (int i = 0; i < o.length; i++) {
			r[i]=(Pedido)o[i];
		}
    	 return  r;
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido[] colaDespachosList()
     {
    	 Object[] o=desp.darCosas();
    	 Pedido[] r= new Pedido[o.length];
    	 for (int i = 0; i < o.length; i++) {
			r[i]=(Pedido)o[i];
		}
     return r;
     }
}
