package taller.mundo;

public class Pedido  implements Comparable<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	
	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	private boolean atendido;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	public Pedido(double p, String a, int c){
		precio=p;
		autorPedido=a;
		cercania=c;
		atendido=false;
	}
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}

	@Override
	public int compareTo(Pedido o) {
		if (atendido){
			if(cercania<o.cercania) return -1;
			else if(cercania>o.cercania) return 1;
			else return 0;
		}
		
		else{
		if(precio<o.precio) return -1;
		else if(precio>o.precio) return 1;
		else return 0;
	}}
	public void atender(){
		atendido=true;
	}
	// TODO 
}
