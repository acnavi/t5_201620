package taller.estructuras;

public class Heap< T extends Comparable<T>> implements IHeap<T > {

	T[] cosas;
	int tamano;
	boolean b;
	
	public Heap(int tInicial, boolean b){
		cosas= (T[])new Comparable[tInicial+1];
		tamano=0;
		this.b=b;
	}
	
	@Override
	public void add(T elemento) {
		// TODO Auto-generated method stub
		tamano++;
		if(cosas.length<=tamano)
			resize();
		cosas[tamano]=elemento;
		siftUp();
		
	}

	public void resize(){
		resize(1);
	}
	public void resize(int n){
		T[] temp= (T[])new Comparable[cosas.length+n];
		for (int i = 1; i < cosas.length; i++) {
            temp[i] = (T) cosas[i];
        }
        cosas = temp;
	}
	
	@Override
	public T peek() {
		
		return (T) cosas[1];
	}

	@Override
	public T poll() {
		T temp= (T) cosas[1];
		cosas[1]=cosas[tamano];
		cosas[tamano]=null;
		tamano--;
		siftDown();
		return temp;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return tamano;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (size()==0);
	}

	@Override
	public void siftUp() {
		// TODO Auto-generated method stub
		int index = size();
        while (index > 1){
            int parent = index / 2;
            if (cosas[index].compareTo( cosas[parent]) >= 0&&b)
                
                break;
            else if (cosas[index].compareTo( cosas[parent]) < 0&&!b)
            	break;
            swap(index,parent);
            index = parent;
        }       
	}

	private void swap(int i, int j) {
		// TODO Auto-generated method stub
		T temp=cosas[i];
		cosas[i]=cosas[j];
		cosas[j]=temp;
	}

	@Override
	public void siftDown() {
		// TODO Auto-generated method stub
		int index = 1;
        while (true){
            int child = index*2;
            if (child > size())
                break;
            if (child + 1 <= size()){
                //if there are two children -> take the smalles or
                //if they are equal take the left one
                child = findMin(child, child + 1);
            }
            if (cosas[index].compareTo(cosas[child]) <= 0 && b)
                break;
            else if(cosas[index].compareTo(cosas[child]) > 0 && !b)
            	break;
            swap(index,child);
            index = child;
        }
		
	}
	private int findMin(int leftChild, int rightChild) {
        if (cosas[leftChild].compareTo(cosas[rightChild]) <= 0&&b)
            return leftChild;
        else if (cosas[leftChild].compareTo(cosas[rightChild]) > 0&&!b)
        	return leftChild;
        else
            return rightChild;
    }
	public Object[] darCosas(){
		/*Object[] c=new Object[tamano];
		for (int i = 1; i <= tamano; i++) {
			c[i]=cosas[i];
		}
		return  c;*/
	return cosas;
	}

	
}
